from django.urls import path

from fcm.api import views

urlpatterns = [
    path("notification/", views.FCMView.as_view()),
]
