from firebase_admin import messaging
from rest_framework import serializers


class ApsSerializer(serializers.Serializer):
    alert = serializers.CharField(max_length=255, allow_null=True)
    badge = serializers.IntegerField(allow_null=True)
    sound = serializers.CharField(max_length=255, allow_null=True)
    content_available = serializers.BooleanField(allow_null=True)
    category = serializers.CharField(max_length=255, allow_null=True)
    thread_id = serializers.CharField(max_length=255, allow_null=True)
    mutable_content = serializers.BooleanField(allow_null=True)

    def to_representation(self, data):
        output = super().to_representation(data)
        return messaging.Aps(**output)


class APNSPayloadSerializer(serializers.Serializer):
    aps = ApsSerializer()

    def to_representation(self, data):
        output = super().to_representation(data)
        return messaging.APNSPayload(**output)


class APNSConfigSerializer(serializers.Serializer):
    payload = APNSPayloadSerializer()

    def to_representation(self, data):
        output = super().to_representation(data)
        return messaging.APNSConfig(**output)


class NotificationSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=255, required=True)
    body = serializers.CharField(max_length=255, required=True)
    image = serializers.CharField(max_length=255, allow_null=True)

    def to_representation(self, data):
        output = super().to_representation(data)
        return messaging.Notification(**output)


class MessageSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=255, required=True)
    notification = NotificationSerializer()
    apns = APNSConfigSerializer()

    def to_representation(self, data):
        output = super().to_representation(data)
        # Because drf tries to create ReturnDict from the return value of this method
        return {"message": messaging.Message(**output)}
