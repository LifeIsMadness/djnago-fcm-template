from drf_spectacular.utils import extend_schema
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from fcm.api.serializers import MessageSerializer
from fcm.services import firebase


class FCMView(APIView):
    """
    Notify user
    """

    permission_classes = [AllowAny]

    @extend_schema(
        request=MessageSerializer,
    )
    def post(self, request, *args, **kwargs):
        msg = MessageSerializer(data=request.data)
        msg.is_valid(raise_exception=True)
        fcm_service = firebase.FirebaseNotificator(**msg.data)
        fcm_service.send()
        return Response({"ok": True})
