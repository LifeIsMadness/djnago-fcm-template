from typing import Optional

from firebase_admin import messaging


class FirebaseNotificator:
    """
    Firebase message sender
    """

    def __init__(
        self,
        message: messaging.Message,
        data: Optional[dict] = None,
    ):
        if data:
            message.data = data
        self.message = message

    def send(self) -> None:
        """
        Sends message to single device via FCM
        """
        messaging.send(self.message)
