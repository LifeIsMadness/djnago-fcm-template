import firebase_admin
from firebase_admin import credentials

from app.conf.environ import env

if not env("PYTEST_DISABLE_FIREBASE", default=False, cast=bool):
    keys = {
        "type": "service_account",
        "project_id": env("FIREBASE_PROJECT_ID", cast=str),
        "private_key_id": env("FIREBASE_PRIVATE_KEY_ID", cast=str),
        "private_key": env("FIREBASE_PRIVATE_KEY", cast=str).replace(r"\n", "\n"),
        "client_email": env("FIREBASE_CLIENT_EMAIL", cast=str),
        "client_id": env("FIREBASE_CLIENT_ID", cast=str),
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": env("FIREBASE_CLIENT_X509_CERT_URL", cast=str),
    }

    creds = credentials.Certificate(keys)
    default_app = firebase_admin.initialize_app(creds)
